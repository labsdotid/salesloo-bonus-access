<?php

namespace Salesloo_Bonus_Access;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    salesloo-bonus-access
 * @subpackage salesloo-bonus-access/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Deactivator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function run()
	{
	}
}
