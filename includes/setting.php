<?php

namespace Salesloo_Bonus_Access;


/**
 * Setting classes
 */
class Setting
{
    public function setting($sections)
    {
        $sections['bonus_access'] = [
            'label' => 'Bonus Access',
            'callback' => [$this, 'setting_view'],
        ];

        return $sections;
    }

    public function setting_view()
    {
        \salesloo_field_toggle([
            'label'           => __('Enable', 'salesloo'),
            'name'            => 'affiliate_bonus_access',
            'description'     => __('Disable or enable affiliate bonus access', 'salesloo-ab'),
            'value'   => \salesloo_get_option('affiliate_bonus_access'),
        ]);

        \salesloo_field_submit();
    }

    public function allowed_0_value($keys)
    {
        $keys[] = 'affiliate_bonus_access';

        return $keys;
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        //add_filter('salesloo/options/allowed_0_value', [$this, 'allowed_0_value']);
        add_filter('salesloo/admin/settings/affiliate/sections', [$this, 'setting']);
    }
}
