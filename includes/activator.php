<?php

namespace Salesloo_Bonus_Access;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Salesloo_Bonus_Access
 * @subpackage Salesloo_Bonus_Access/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function run()
	{
	}
}
