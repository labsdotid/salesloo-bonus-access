<?php

namespace Salesloo_Bonus_Access;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Frontend classes
 */
class Frontend
{

    public function add_section()
    {

        $textarea = salesloo_get_product_meta(___salesloo('product')->ID, 'bonus_access_' . get_current_user_id(), true);

        if (empty(salesloo_get_option('affiliate_bonus_access'))) return;
        ob_start();
?>
        <div class="text-gray-600">
            <div class="flex items-center py-5 border-b border-gray-100">
                <div class="w-full">
                    <div class="flex items-center justify-center">
                        <div class="mr-2 relative">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4" />
                            </svg>
                        </div>
                        <h2 class="font-semibold text-lg mr-auto"><?php _e('Bonus Access', 'salesloo'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-sm flex flex-col px-5 sm:px-10">
                <div class="py-5 sm:py-10">
                    <div class="text-sm text-gray-400">
                        <?php _e('Your bonus access information', 'salesloo'); ?>
                    </div>
                    <div class="flex space-y-2 justify-center items-center flex-col">
                        <div class="w-full">
                            <div class="flex items-center justify-end flex-row">
                                <button class="text-gra-500 text-sm cursor-pointer font-bold inline-flex space-x-2" @click="bonus_access.value = JSON.stringify(tinyMCE.activeEditor.getContent({format : 'raw'}));$post('', bonus_access).then(res=>{bonus_access = res.data});">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                        </svg>
                                    </span><span><?php _e('Save', 'salesloo'); ?></span>
                                </button>
                            </div>
                        </div>
                        <div class="w-full text-gray-400">
                            <textarea id="bonuseditor" class="hidden"><?php echo $textarea; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
        echo ob_get_clean();
    }

    public function replace_link($tag)
    {

        $new_tag = str_replace('<a ', '<a target="__blank" ', $tag[0]);
        return $new_tag;
    }

    public function bonus_access()
    {
        $product = ___salesloo('order')->get_product();
        $affiliate_id = ___salesloo('order')->get_affiliate_id();

        $bonus = salesloo_get_product_meta($product->ID, 'bonus_access_' . $affiliate_id, true);

        if (empty($bonus)) return;

        $bonus = html_entity_decode($bonus);

        $bonus = preg_replace_callback('/<a([^>]+)>(.+?)<\/a>/s', [$this, 'replace_link'], $bonus);

        if (empty(salesloo_get_option('affiliate_bonus_access'))) return;
    ?>
        <div class="text-gray-600">
            <div class="flex items-center py-5 border-b border-gray-100">
                <div class="w-full">
                    <div class="flex items-center justify-center">
                        <div class="mr-2 relative">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4" />
                            </svg>
                        </div>
                        <h2 class="font-semibold text-lg mr-auto"><?php _e('Affiliate Bonus Access', 'salesloo'); ?></h2>
                    </div>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-sm flex flex-col px-5 sm:px-10">
                <div class="py-5 sm:py-10">
                    <div class="flex space-y-2 justify-center items-center flex-col">
                        <div class="w-full text-gray-400">
                            <?php echo apply_filters('the_content', html_entity_decode($bonus)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php
    }

    public function enqueue_scripts($scripts)
    {
        if (empty(salesloo_get_option('affiliate_bonus_access'))) return $scripts;

        $scripts[] = [
            'id' => 'tinymce',
            'source' => 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js',
            'defer' => false
        ];

        return $scripts;
    }

    public function inject_to_footer()
    {

        if (__is_salesloo_page() != 'affiliate/product/id' || empty(salesloo_get_option('affiliate_bonus_access'))) return;

    ?>
        <script>
            tinymce.init({
                selector: '#bonuseditor',
                height: 500,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount',
                    'autosave'
                ],
                toolbar: 'undo redo | formatselect | ' +
                    'bold italic backcolor | alignleft aligncenter ' +
                    'alignright alignjustify | bullist numlist outdent indent | ' +
                    'removeformat | link ',
                content_style: 'body { font-family:inherit; font-size:16px }',
                autosave_ask_before_unload: false,
                autosave_interval: '10s',
                autosave_prefix: 'idzxcx',
                autosave_restore_when_empty: false,
                autosave_retention: '2m',
            });
        </script>
<?php
    }

    public function action($request)
    {
        $product_slug = salesloo_get_current_product_slug();
        $product = salesloo_get_product_by('slug', $product_slug);

        $meta_key     = 'bonus_access_' . get_current_user_id();

        if ($request->action == 'update_bonus_access') {
            $response = new \Salesloo\Response();

            $value = $request->value;
            $value = json_decode($value);
            $value = salesloo_scure_html($value);
            salesloo_update_product_meta($product->ID, $meta_key, $value);

            $data = [
                'message' => __('Bonus access has updated', 'salesloo'),
                'data' => [
                    'value' => $value,
                    'nonce' => salesloo_create_nonce(),
                    'action' => 'update_bonus_access'
                ]
            ];

            $response->set_data($data);
            $response->set_status(200);

            $response->json();
            exit;
        }
    }

    public function load()
    {
        global $___salesloo;

        if (empty(salesloo_get_option('affiliate_bonus_access'))) return;

        $___salesloo['data']['bonus_access'] = [
            'value' => '',
            'nonce' => salesloo_create_nonce(),
            'action' => 'update_bonus_access'
        ];
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_action('salesloo/frontend/affiliate/product/id/action', [$this, 'action']);
        add_action('salesloo/affiliate/product/end', [$this, 'add_section']);
        add_action('salesloo/order/access/after', [$this, 'bonus_access']);
        add_filter('salesloo/frontend/affiliate/product/id/scripts', [$this, 'enqueue_scripts']);
        add_action('salesloo/footer', [$this, 'inject_to_footer']);
        add_action('salesloo/affiliate/product/load', [$this, 'load']);
    }
}
